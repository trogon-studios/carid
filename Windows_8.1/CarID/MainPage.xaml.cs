﻿using CarID.Common;
using CarID.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace CarID {
	/// <summary>
	/// A page that displays details for a single item within a group.
	/// </summary>
	public sealed partial class MainPage : Page {
		private readonly NavigationHelper navigationHelper;
		private readonly ObservableDictionary defaultViewModel = new ObservableDictionary( );

		public MainPage( ) {
			this.InitializeComponent( );

			this.navigationHelper = new NavigationHelper(this);
			this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
			this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
		}

		/// <summary>
		/// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
		/// </summary>
		public NavigationHelper NavigationHelper {
			get { return this.navigationHelper; }
		}

		/// <summary>
		/// Gets the view model for this <see cref="Page"/>.
		/// This can be changed to a strongly typed view model.
		/// </summary>
		public ObservableDictionary DefaultViewModel {
			get { return this.defaultViewModel; }
		}

		/// <summary>
		/// Populates the page with content passed during navigation. Any saved state is also
		/// provided when recreating a page from a prior session.
		/// </summary>
		/// <param name="sender">
		/// The source of the event; typically <see cref="NavigationHelper"/>.
		/// </param>
		/// <param name="e">Event data that provides both the navigation parameter passed to
		/// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
		/// a dictionary of state preserved by this page during an earlier
		/// session.  The state will be null the first time a page is visited.</param>
		private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
			// TODO: Create an appropriate data model for your problem domain to replace the sample data.
			var item = await CarIdDataSource.GetItemAsync((string)e.NavigationParameter);
			this.DefaultViewModel["Item"] = item;
			this.idTextBox_TextChanged(null, null);
		}

		/// <summary>
		/// Preserves state associated with this page in case the application is suspended or the
		/// page is discarded from the navigation cache.  Values must conform to the serialization
		/// requirements of <see cref="SuspensionManager.SessionState"/>.
		/// </summary>
		/// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/>.</param>
		/// <param name="e">Event data that provides an empty dictionary to be populated with
		/// serializable state.</param>
		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
			// TODO: Save the unique state of the page here.
		}

		#region NavigationHelper registration

		/// <summary>
		/// The methods provided in this section are simply used to allow
		/// NavigationHelper to respond to the page's navigation methods.
		/// <para>
		/// Page specific logic should be placed in event handlers for the  
		/// <see cref="NavigationHelper.LoadState"/>
		/// and <see cref="NavigationHelper.SaveState"/>.
		/// The navigation parameter is available in the LoadState method 
		/// in addition to page state preserved during an earlier session.
		/// </para>
		/// </summary>
		/// <param name="e">Provides data for navigation methods and event
		/// handlers that cannot cancel the navigation request.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e) {
			this.navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e) {
			this.navigationHelper.OnNavigatedFrom(e);
		}

		#endregion

		private async void idTextBox_TextChanged(object sender, TextChangedEventArgs e) {

			var textBox = sender as TextBox;
			if (textBox != null) {
				this.provinceName.Text = "nieznane";
				this.districtName.Text = "nieznany";
				var key = textBox.Text.Trim( );
				if (key.Length > 0) {
					var provice = await CarIdDataSource.GetGroupAsync(key.Substring(0, 1));
					if (provice != null) {
						this.provinceName.Text = provice.Name;
						CarIdDataItem district = null;
						if (key.Length > 2) {
							district = await CarIdDataSource.GetItemAsync(key.Substring(0, 3));
						}
						if (district == null && key.Length > 1) {
							district = await CarIdDataSource.GetItemAsync(key.Substring(0, 2));
						}
						if (district != null) {
							this.districtName.Text = district.Name;
						}
					}
				}
			}
		}

		private void idTextBox_KeyDown(object sender, KeyRoutedEventArgs e) {
			var textBox = sender as TextBox;
			if (textBox != null
				&& (int)e.Key >= (int)Windows.System.VirtualKey.A
				&& (int)e.Key <= (int)Windows.System.VirtualKey.Z) {

				textBox.Text += e.Key.ToString( );
				textBox.SelectionStart = textBox.Text.Length;
				textBox.SelectionLength = 0;

				e.Handled = true;
			}
		}
	}
}
